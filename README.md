## Keyboard

This is a free keyboard design project

### Keyboards:

### Getting Started

Install [Kicad](http://kicad-pcb.org/)

Open the component library manager and install the components.

Open the footprint library manager and install the footprints.

### License

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

