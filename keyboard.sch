EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:keyboard
LIBS:keyboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Keyboard Switch Testing Schematic"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MX_DG SW1
U 1 1 593BBAE3
P 6150 4100
F 0 "SW1" H 6150 4000 60  0000 C CNN
F 1 "MX_DG" H 6150 4150 60  0000 C CNN
F 2 "keyboard:MX1A-X1DGW" H 5900 4270 60  0001 C CNN
F 3 "" H 5900 4270 60  0001 C CNN
	1    6150 4100
	1    0    0    -1  
$EndComp
Text Notes 6000 4650 0    60   ~ 0
SAMPLE\nNOT AN ACTUAL SWITCH
$Comp
L BREAKOUT IO1
U 1 1 593BBDD7
P 7400 4350
F 0 "IO1" H 7450 4200 60  0000 C CNN
F 1 "BREAKOUT" H 7450 4500 60  0000 C CNN
F 2 "keyboard:BREAKOUT" H 7400 4350 60  0001 C CNN
F 3 "" H 7400 4350 60  0001 C CNN
	1    7400 4350
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 4300 7050 4300
Wire Wire Line
	6350 4400 6750 4400
Wire Wire Line
	6750 4400 6750 4200
Wire Wire Line
	6750 4200 7050 4200
Wire Wire Line
	6350 4150 6850 4150
Wire Wire Line
	6850 4150 6850 4400
Wire Wire Line
	6850 4400 7050 4400
Wire Wire Line
	6050 3950 7750 3950
Wire Wire Line
	7750 3950 7750 4400
Wire Wire Line
	7750 4400 7650 4400
Wire Wire Line
	5950 4400 5950 4700
Wire Wire Line
	5950 4700 7700 4700
Wire Wire Line
	7700 4700 7700 4200
Wire Wire Line
	7700 4200 7650 4200
Wire Wire Line
	5950 4300 5900 4300
Wire Wire Line
	5900 4300 5900 4750
Wire Wire Line
	5900 4750 7800 4750
Wire Wire Line
	7800 4750 7800 4300
Wire Wire Line
	7800 4300 7650 4300
$EndSCHEMATC
